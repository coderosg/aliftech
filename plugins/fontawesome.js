import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserFriends, faChartLine, faPen, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons'

// https://github.com/FortAwesome/vue-fontawesome#nuxtjs

// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(faUserFriends)
library.add(faChartLine)
library.add(faPen)
library.add(faTrash)
library.add(faTimes)

// Register the component globally
Vue.component('font-awesome-icon', FontAwesomeIcon)
