import Vue from 'vue'

export default (context, inject) => {
  inject('confirmDialog', (confirmMessage, params) => {
    const vm = new Vue()
    return vm.$bvModal.msgBoxConfirm(confirmMessage || 'Вы действительно хотите удалить этот запись?', {
      title: 'Пожалуйста подтвердите',
      size: 'md',
      buttonSize: 'sm',
      okVariant: 'danger',
      okTitle: 'Да',
      cancelTitle: 'Нет',
      footerClass: 'p-2',
      hideHeaderClose: false,
      centered: true,
      ...params
    })
  })
}
