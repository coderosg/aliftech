export const parseRequestLink = (data) => {
  if (!data) { return }
  const parsedData = {}
  const arrData = data.split(',')
  let linkInfo = ''

  for (const param of arrData) {
    linkInfo = /<([^>]+)>;\s+rel="([^"]+)"/ig.exec(param)
    parsedData[linkInfo[2]] = linkInfo[1]
  }

  return parsedData
}
