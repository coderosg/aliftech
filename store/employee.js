import { parseRequestLink } from '~/helpers/request-parser'

export const state = () => ({
  list: null,
  detail: null
})

export const mutations = {
  SET_EMPLOYEE_DETAIL (state, payload) {
    state.detail = payload
  },
  SET_EMPLOYEE_LIST (state, payload) {
    state.list = payload
  }
}

export const actions = {
  getEmployeeDetailAction ({ commit, state }, id) {
    commit('SET_EMPLOYEE_DETAIL')
    this.$axios.get(`employees/${id}`).then((response) => {
      commit('SET_EMPLOYEE_DETAIL', response.data)
    })
  },
  addEmployeeAction ({ commit, state }, data) {
    return this.$axios.post('employees', data)
  },
  updateEmployeeAction ({ commit, state }, { id, ...data }) {
    return this.$axios.patch(`employees/${id}`, data)
  },
  deleteEmployeeAction ({ commit, state }, id) {
    return this.$axios.delete(`employees/${id}/`)
  },
  getEmployeeListAction ({ commit, state }, params) {
    commit('SET_EMPLOYEE_LIST')
    this.$axios.get('employees', {
      params: {
        _page: 1,
        _limit: 10,
        ...params
      }
    }).then((response) => {
      const { data, headers } = response
      const count = parseInt(headers['x-total-count'])
      const pagination = parseRequestLink(headers.link)
      commit('SET_EMPLOYEE_LIST', {
        ...pagination,
        count,
        data
      })
      return data
    })
  }
}

export const getters = {}
