import { createLogger } from 'vuex'

export const state = () => ({
  dashboard: null
})

export const mutations = {
  SET_DASHBOARD_INFO (state, payload) {
    state.dashboard = payload
  }
}

export const actions = {
  getDashboardInfoAction ({ commit, state }, id) {
    commit('SET_DASHBOARD_INFO')
    this.$axios.get('chart/employee').then((response) => {
      commit('SET_DASHBOARD_INFO', response.data)
    })
  }
}

export const getters = {}

export const plugins =
  process.env.NODE_ENV !== 'production' ? [createLogger()] : []
