export const DEPARTMENT_LIST = [
  { value: null, text: 'Выберите отдел' },
  {
    value: 1,
    text: 'Бухгалтерия'
  },
  {
    value: 2,
    text: 'IT'
  },
  {
    value: 3,
    text: 'Маркетинг'
  }
]
export const DEPARTMENTS = {
  1: 'Бухгалтерия',
  2: 'IT',
  3: 'Маркетинг'
}
