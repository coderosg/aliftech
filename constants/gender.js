export const GENDER_LIST = [
  { value: null, text: 'Выберите пол' },
  {
    value: 1,
    text: 'Мужчина'
  },
  {
    value: 2,
    text: 'Женщина'
  }
]

export const GENDERS = {
  1: 'Мужчина',
  2: 'Женщина'
}
